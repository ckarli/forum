from .models import Category, Post
from rest_framework.serializers import ModelSerializer, PrimaryKeyRelatedField,BooleanField

class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ("id","name",)

class PostSerializer(ModelSerializer):
    is_approved = BooleanField(initial=True)
    post_category = PrimaryKeyRelatedField(many=False,queryset=Category.objects.all())

    class Meta:
        model = Post
        fields = ("id","subject","body","post_date","edit_date","is_approved","post_category")

