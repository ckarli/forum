import django_filters
from datetime import date
from rest_framework import filters

from rest_framework.viewsets import ModelViewSet
from .serializer import CategorySerializer,PostSerializer
from .models import Category,Post


class PostsDateFilter(django_filters.FilterSet):
   post_date = django_filters.DateTimeFilter(name="post_date",lookup_type="exact")

   class Meta:
       model = Post
       fields = ['post_date']


class CategoryViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class PostViewSet(ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_fields = ('post_date','post_category')





