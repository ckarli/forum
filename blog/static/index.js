const { Router, Route, Link, browserHistory } = ReactRouter
const {Component} = React;

class CategoryContainer extends Component{
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }

  componentDidMount() {
    const url= "/api/categories/";
    $.ajax({
      url: url,
      dataType: 'json',
      success: (data) => {
        this.setState({data: data});
      },
      error: (xhr, status, err) => {
        console.error(url, status, err.toString());
      }
    })
  }


  render() {
    return (
      <div>

        <h2>Categories</h2>

          <ul>
            {this.state.data.map(data => (<CategoryItem key={data.id} data={data} />))}
          </ul>

      </div>
    )
  }



}




class CategoryPostContainer extends Component{
    constructor(props) {
        super(props)
        this.state = {
            data: []
        }
    }
    componentDidMount() {
            $.ajax({
              url: '/api/posts/?post_category=' + this.props.params.id,
              dataType: 'json',
              success: (data) => {
                this.setState({data: data});
              },
              error: (xhr, status, err) => {
                console.error(url, status, err.toString());
              }
        })
  }

  render() {
    return (
      <div>

        <h2>Posts</h2>

          <ul>
            {this.state.data.map(data => (<CategoryPostItem key={data.id} data={data} />))}
          </ul>

      </div>
    )
  }



}


const CategoryItem = (props) =>
  <tr>
    <td width="80%">
      <Link to= {"/categories/"+props.data.id}> {props.data.name} </Link>
    </td>
  </tr>;

const CategoryPostItem = (props) =>
  <tr>
    <td width="80%">
        {props.data.subject}
    </td>
  </tr>;

ReactDOM.render((
   <Router history={browserHistory}>
    <Route path="/" component={CategoryContainer} />
    <Route path="categories/:id" component={CategoryPostContainer}/>
  </Router>),
  document.getElementById('root')
)