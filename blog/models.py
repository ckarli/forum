from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Category(models.Model):
    name = models.CharField(max_length=50)

    def __unicode__(self):
        return self.name

class Post(models.Model):
    subject = models.CharField(max_length=50)
    body = models.TextField()
    post_date = models.DateField(auto_now_add=True)
    edit_date = models.DateField(auto_now=True)
    is_approved = models.BooleanField(default=True)
    post_category = models.ForeignKey(Category)

    def __unicode__(self):
        return self.subject


